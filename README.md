# README #

This repository is hosted for a website used in my senior thesis. You can see my site by clicking [here][https://morilyn.bitbucket.io].  
If you have any problem and want to contact me, you can contact me on:

* Instagram:[@morilyn (currently susd:/)][https://instagram.com/m0rilyn]
 * Now that one of my insta accounts is killed, I changed my alt to main:[@morilynlol][https://instagram.com/morilynlol]
* Twitter: https://twitter.com/morilynlol (dead)

こんにちは！このリポジトリは卒業研究用のウェブサイトです。閲覧にはこのサイトにアクセスしてください：[https://morilyn.bitbucket.io]
もし何か連絡がありましたら上記InstagramかTwitterにお願いします！

## 受講生の方へ ##
何か問題が起こった際の連絡はTeamsかGmail, もしくはLINEでお願いします。Teamsの場合は講座のTeamから僕のプロフィールにアクセスするか, ユーザー検索ボックスに名前を入力してください。